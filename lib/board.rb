class Board
  def initialize
    @cells = []
    (1..3).each do |x|
      (1..3).each do |y|
        @cells << Cell.new(Position.new(x, y))
      end
    end
    
  end

  def mark(position, player)
    @cells.each {|cell| cell.mark(player) if cell.position == position}
  end

  def game_over?
    # if y== 1 and 2 and are marked by the same playerd and the x same
    @cells.any? do |cell_under_consideration|
      x = cell_under_consideration.position.x
      player = cell_under_consideration.marked_by
      @cells.all? {|cell_to_compare_to| cell_to_compare_to.position.x == x && cell_to_compare_to.marked_by == player} 
      # dear cell, what is your x coordinate? and who marked you?
      # are all of the other cells with that x coordinate marked by that same wonderful player?
      # if so, return true
    end
  end
end
