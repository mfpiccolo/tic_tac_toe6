class Cell
  attr_reader :marked_by, :position

  def initialize(position)
    @position = position
  end

  def mark(player)
    @marked_by = player
    
  end
end