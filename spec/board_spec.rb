require '../lib/board'
require '../lib/position' 
require '../lib/player'
require '../lib/cell'


board = Board.new
position = Position.new(1, 1)
player = Player.new("X")

puts "#{board} should be an instance of Board."

"#{board.mark(position, player)} change a spot on a board to marked by X."

# puts "#{board.to_s} should be a graphical representation of the board"

puts "#{board.game_over?} should be false"

[Position.new(1, 2), Position.new(1, 3)].each {|position| board.mark(position, player)}
puts "#{board.game_over?} should be true"