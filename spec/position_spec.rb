require '../lib/position'

position = Position.new(1, 1)
puts "#{position} should be an instance of Position"

puts "#{position.x} should be 1"
puts "#{position.y} should be 1"