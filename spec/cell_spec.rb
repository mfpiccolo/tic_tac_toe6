require '../lib/cell'
require '../lib/position'
require '../lib/player'

position = Position.new(1, 1)
cell = Cell.new(position)
player = Player.new('X')

puts "#{cell} should be an instance of Cell"

puts "#{cell.marked_by} should be nil"

cell.mark(player)
puts "#{cell.marked_by} should be #{player}"

puts "#{cell.position} should be #{position}"