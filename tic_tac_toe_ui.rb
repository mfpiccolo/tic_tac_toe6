
board = Board.new
players = [Player.new("X"), Player.new("O")]
puts "Welcome to Tic Tac Toe"

until board.game_over?
  players.each do |player|
    puts "#{player.mark}, your turn."
    # puts board
    puts "Where would you like to place your mark?"
    puts "Enter the x coordinate:"
    x = gets.chomp.to_i
    puts "Enter the y coordinate:"
    y = gets.chomp.to_i
    puts "Enter the z coordinate:"
    z = gets.chomp.to_i
    position = Position.new(x, y)
    board.mark(position, player)
  end
end